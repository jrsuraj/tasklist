@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Task
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    {{-- <form action="{{ url('task') }}" method="POST" class="form-horizontal"> --}}
                        

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Task</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="task-name" class="form-control" value="{{ old('task') }}">
                            </div>
                            {{ csrf_field() }}
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="button" id="addNew" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Task
                                </button>
                            </div>
                        </div>
                        
                    {{-- </form> --}}
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($tasks) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Tasks
                    </div>

                    <div class="panel-body" id="currenttasks">
                        <table class="table table-striped task-table">
                            <thead>
                                <th>Task</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $task)
                                    <tr class="eachtask">
                                        <td class="table-text">
                                            <div class="taskname">{{ $task->name }}
                                                <input type="hidden" id="taskid" name="taskid" value="{{$task->id}}">
                                            </div>
                                        </td>

                                        <!-- Task Delete Button -->
                                        <td>
                                            {{-- <form action="{{url('task/' . $task->id)}}" method="POST"> --}}
                                                {{ csrf_field() }}
                                                {{-- {{ method_field('DELETE') }} --}}

                                                {{-- <button type="button" id="delete-task-{{ $task->id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button> --}}
                                                <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#confirmmodal">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            {{-- </form> --}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="modal fade" id="confirmmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure to delete?
            </div>
            <div class="modal-footer">
              <button type="button" id="delete" class="btn btn-danger" data-dismiss="modal">Delete</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>
@endsection
